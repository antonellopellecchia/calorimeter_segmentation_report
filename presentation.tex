\documentclass{beamer}
\usepackage{lmodern}
\usepackage{forloop}
\usepackage{graphicx}

\usecolortheme{wolverine}

\graphicspath{ {images/} }

\title[depth_project]{Calorimeter depth segmentation using timing information}
\author[Pellecchia]{Antonello Pellecchia}
\institute{
	University of Bari - Italy
	\and
	SLAC/INFN summer exchange program
}
\date[09/21/2018]{September 21, 2018}

\begin{document}
	\begin{frame}
		\maketitle
		Supervisor: Prof. Charles Young
	\end{frame}

	\begin{frame}\frametitle{A broad problem in experimental physics}
		\centering{\includegraphics[width=\textwidth]{images/convolution.png}}
	\end{frame}

	\begin{frame}\frametitle{Light signals in electromagnetic calorimeters}
		\centering{\includegraphics[width=\textwidth]{images/calorimeter_scheme.png}}
		\[f_{response} = f_{light}*f_{calibration}\]
	\end{frame}

	\begin{frame}\frametitle{Cumulants and additivity property}
		\framesubtitle{Characterizing signals by looking for linear properties}
		\[
			h(x) = (f*g)(x) = \int_{-\infty}^{+\infty} dy\, f(y)g(x-y)
			\Rightarrow
			c_h^{(k)} = c_f^{(k)}+c_g^{(k)}
		\]
		Moments give the moment-generating function:
		\[m_f^{(k)} = \int_{-\infty}^{\infty}dx\,x^kf(x)\]
		\[\phi_X(\omega) = \sum_{k=0}^\infty \frac{(i\omega t)^k}{k!}m_X^{(k)}\]

		Cumulants are obtained by the cumulant-generating function:
		\[\kappa(\omega) = \log(\phi_X(\omega)) = \sum_{n=0}^\infty \frac{(i\omega t)^n}{n!}c_X^{(n)}\]

		Moment-cumulant relation:
		\[c_n = m_n-\sum_{k=1}^{n-1}\binom{n-1}{k}m_k~c_{n-k}\]
	\end{frame}

	\begin{frame}\frametitle{Light signals in electromagnetic calorimeters}
		\centering{\includegraphics[width=\textwidth]{images/calorimeter_scheme.png}}
		\[f_{response} = f_{light}*f_{calibration} \Rightarrow c_{light}^{(k)} = c_{response}^{(k)}-c_{calibration}^{(k)}\]
		Can we obtain $f_{light}$ back from $c_{light}^{(k)}$?
	\end{frame}

	\begin{frame}\frametitle{Convolution tests in practice}
		\begin{itemize}[<+->]
			\item Select a pair of distributions $f,g$ from a pool
			\item Calculate their Monte Carlo convolution $h$
			\item Calculate sequences of cumulants for $f,g$ and $h$
			\item Plot cumulant discrepancy $c_f+c_g-c_h$
		\end{itemize}
	\end{frame}

	\begin{frame}\frametitle{Convolution tests: cumulant discrepancies}
		\begin{overprint}
			\onslide<1>
			\centering{\includegraphics[width=.7\textwidth]{graphs/convolution_tests/overview_Uniform.png}}
			\onslide<2>
			\centering{\includegraphics[width=\textwidth]{graphs/convolution_tests/overview.png}}
			\onslide<3>
			\centering{\includegraphics[width=\textwidth]{graphs/convolution_tests/overview_relative.png}}
		\end{overprint}
	\end{frame}

	\begin{frame}\frametitle{From shower shape to light distribution}
		\begin{columns}
			\begin{column}{.4\textwidth}
				\centering{\includegraphics[width=1.2\textwidth]{graphs/pmt_signal/light_dist.png}}
				\pause
				\tiny
				\[N_\gamma(X) = \frac{0.31}{\sqrt\beta}\exp\left[X\left(1-\frac{3}{2}\log\frac{3X}{X+2\beta}\right)\right]\]

				\centering{\includegraphics[width=1.2\textwidth]{images/light_dist_production.png}}
			\end{column}
			\pause
			\begin{column}{.6\textwidth}
				\tiny\[t = \frac{L}{v}+\left(\frac{1}{c}-\frac{1}{v}\right)x\]
				\centering{\includegraphics[width=\textwidth]{images/graphs/pmt_signal/light_dist_t.png}}
				\begin{minipage}{.5\textwidth}
					\bigskip
					PWO calorimeter\\
					$X_0=0.92cm@8.28g/cm^3$\\
					Depth $25\,X_0$\\
					$\beta=1, n=3$\\
				\end{minipage}
			\end{column}
		\end{columns}
	\end{frame}

	\begin{frame}\frametitle{From shower shape to light distribution}
		\centering{\includegraphics[width=\textwidth]{images/graphs/pmt_signal/greisen.png}}
	\end{frame}

	\begin{frame}\frametitle{Photo-sensor response}
		\begin{center}
			\includegraphics[width=.8\textwidth]{images/graphs/pmt_signal/spr_pmt.png}
		\end{center}
		\begin{center}
			56 DVP-03 photomultiplier\footnote{Bianchetti, G. and Righini, B. (1972). Photo-multipliers output pulse shape. Nuclear Instruments and Methods}
		\end{center}
	\end{frame}

	\begin{frame}\frametitle{Testing signal reconstruction}
		\begin{minipage}{\textwidth}
		%\includegraphics[width=.25\textwidth]{images/graphs/pmt_signal/light_dist.png}
			\includegraphics[width=.5\textwidth]{images/graphs/pmt_signal/light_dist_t.png}
			\includegraphics[width=.5\textwidth]{images/graphs/pmt_signal/spr_pmt.png}
		\end{minipage}
		\pause
		\begin{columns}
			\begin{column}{.5\textwidth}
				\includegraphics[width=\textwidth]{images/graphs/pmt_signal/pmt.png}
			\end{column}
			\begin{column}{.5\textwidth}\tiny
			\pause
				Four sequences of cumulants:
				\begin{itemize}
					\item Response function ($c_R$)
					\item Single-photon response ($c_r$)
					\item Light distribution ($c_L$)
					\\ \smallskip to be compared with
					\item $c'_L=c_R-c_r$
				\end{itemize}
				We expect $c'_L=c_L$
			\end{column}
		\end{columns}
	\end{frame}

	\begin{frame}\frametitle{Cumulant comparison}
		\includegraphics[width=\textwidth]{images/graphs/pmt_signal/pmt_discrepancies.png}
	\end{frame}

	\begin{frame}\frametitle{Approximating a shape from a sequence of cumulants}
		Different reconstruction techniques:
		\begin{itemize}
			\item Inverse Fourier transformation
			\begin{itemize}\item computationally intensive\end{itemize}
			\item Fit by known shape
			\begin{itemize}\item limited scope of validity\end{itemize}
			\item Orthogonal polynomial expansion
		\end{itemize}
	\end{frame}

	\begin{frame}\frametitle{Jacobi polynomials}
		\begin{itemize}
			\item Orthogonal polynomials in $[-1,1]$:
			\[G_i(s) = \sum_{j=1}^iG_{ij}\,s^j ~~~~~ \int_{-1}^1ds\, G_i(s)\,G_j(s) = \delta_{ij}\]
			\item Basis for $L^2$:
			\[f(s) = \sum_{j=0}^\infty\lambda_j\,G_j(s)\]
			\item The $\lambda_i$'s are combination of the first $i$ moments:
			\[\lambda_i = \sum_{j=0}^iG_{ij}\,m_j\]
		\end{itemize}
		Distribution expansion in terms of its moments:
		\[f(s) = \sum_{i=0}^\infty\Bigg(\sum_{j=0}^iG_{ij}\,m_j\Bigg)G_i(s)\]
	\end{frame}

	\begin{frame}\frametitle{Result: shower shape reconstruction}
		\framesubtitle{Cumulants up to order 8}
		\begin{columns}
			\begin{column}{.55\textwidth}
				\centering\includegraphics[width=\textwidth]{images/graphs/pmt_signal/light_comparison.png}
			\end{column}
			\begin{column}{.45\textwidth}
				\scriptsize Computational trade-off:
				\begin{itemize}\tiny
					\item Monte Carlo convolution precision (influences cumulant discrepancies)
					\\vs
					\item Highest cumulant order (determines reconstruction precision)
				\end{itemize}
				\includegraphics[width=\textwidth]{images/graphs/pmt_signal/recreated_discrepancies.png}
			\end{column}
		\end{columns}
	\end{frame}

%	\begin{frame}\frametitle{Shower shape reconstruction}
%		\begin{overprint}
%			\forloop{ct}{2}{\value{ct} < 15}{
%				\onslide<\value{ct}>\includegraphics[width=\textwidth]{images/graphs/pmt_signal/recreated_discrepancies_\value{ct}.png}
%			}
%		\end{overprint}
%	\end{frame}

	\begin{frame}\frametitle{Shower shape reconstruction}
		\begin{overprint}
			\onslide<1>\includegraphics[width=\textwidth]{images/graphs/pmt_signal/light_comparison_2.png}
			\onslide<2>\includegraphics[width=\textwidth]{images/graphs/pmt_signal/light_comparison_3.png}
			\onslide<3>\includegraphics[width=\textwidth]{images/graphs/pmt_signal/light_comparison_4.png}
			\onslide<4>\includegraphics[width=\textwidth]{images/graphs/pmt_signal/light_comparison_5.png}
			\onslide<5>\includegraphics[width=\textwidth]{images/graphs/pmt_signal/light_comparison_6.png}
			\onslide<6>\includegraphics[width=\textwidth]{images/graphs/pmt_signal/light_comparison_7.png}
			\onslide<7>\includegraphics[width=\textwidth]{images/graphs/pmt_signal/light_comparison_8.png}
			\onslide<8>\includegraphics[width=\textwidth]{images/graphs/pmt_signal/light_comparison_9.png}
			\onslide<9>\includegraphics[width=\textwidth]{images/graphs/pmt_signal/light_comparison_10.png}
			\onslide<10>\includegraphics[width=\textwidth]{images/graphs/pmt_signal/light_comparison_11.png}
			\onslide<11>\includegraphics[width=\textwidth]{images/graphs/pmt_signal/light_comparison_12.png}
			\onslide<12>\includegraphics[width=\textwidth]{images/graphs/pmt_signal/light_comparison_13.png}
			\onslide<13>\includegraphics[width=\textwidth]{images/graphs/pmt_signal/light_comparison_14.png}
		\end{overprint}
	\end{frame}

	\begin{frame}\frametitle{Shower shape reconstruction}
		When looking for depth segmentation, integrated values matter most
		\begin{itemize}\item Small fluctuations about the actual distribution cancel out\end{itemize}
	\end{frame}

	\begin{frame}\frametitle{Summary}
		\begin{itemize}
			\item The longitudinal shower shape in an omogeneous calorimeter is linked to the photosensor output signal by a convolution operation
			\item Cumulants can uniquely characterize the shape
			\item Their linearity with respect to convolution can be exploited in practice to recover the shower shape
			\item Cumulants up to a finite (low) order can yield an approximation of the cascade development
			\item In a simplified case, depth segmentation can be achieved through timing information
			\item Follow-up: an estimation of the uncertainties in the procedure
		\end{itemize}
	\end{frame}
\end{document}