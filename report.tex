\documentclass[10pt,oneside,a4paper]{article}
\usepackage{amsmath}
\usepackage{appendix}
\usepackage{siunitx}
\usepackage[nottoc]{tocbibind}
\usepackage{graphicx}
\graphicspath{ {../code/graphs/} }

\begin{document}

\title{Calorimeter depth segmentation using timing information}
\author{Antonello Pellecchia \\University of Bari\\ SLAC/INFN summer exchange program}
\date{September 2018}

\let\endtitlepage\relax
\begin{titlepage}
	\centering
	{\scshape\large SLAC/INFN summer exchange program \par Final report\par}
	\vspace{.3cm}
	{\large July - September 2018\par}
	\vspace{.5cm}
	{\huge\bfseries Calorimeter depth segmentation using timing information\par}
	\vspace{.5cm}
	\begin{minipage}{.48\textwidth}
		{\Large Antonello Pellecchia\\ University of Bari, Italy\par}
	\end{minipage}	
	\begin{minipage}{.48\textwidth}
		\begin{flushright}
			{\Large supervised by\par Prof. Charles Young\par}
		\end{flushright}
	\end{minipage}\par
	\vspace{.7cm}
\end{titlepage}

\begin{abstract}
\emph{The possibility of recovering the shape of an electromagnetic shower in an omogeneous calorimeter from timing signal information is discussed and a simplified implementation is developed exploiting the linearity of cumulants of distributions. Different reconstruction techniques are tested and compared.}
\end{abstract}

\section{Introduction}
The reconstruction of the shape of an electromagnetic cascade in a calorimeter can be framed as a specific case in a broader set of problems in experimental physics, in which the outcome of a measurement is the combined effect of the underlying physical process and the instrumentation apparatus. The reconstruction technique based on cumulants described here, in particular, discusses whether a physical signal precisely described by an infinite sequence of quantities can be approximated in a satisfactory way by a finite set of other quantities.

In the following sections, at first the formation of a photosensor signal from an electromagnetic cascade in a calorimeter is introduced. After that, the property of linearity of cumulants of distributions is tested in a Monte Carlo simulation; finally, the same property is applied in a simplified case to reconstruct the shape of a shower from the readout signal.

\section{Light distribution in a calorimeter}
Energy loss for incident particles on an omogeneous electromagnetic calorimeter is mainly driven by electromagnetic showers; the shape of a shower is the number of particles produced at a certain point as a function of the depth inside the detector.

If a photosensor is placed at one end of the calorimeter, scintillation light traveling to its input generates a response described by a convolution function:
\[
	R(t) = \int_{-\infty}^{+\infty} ds\, l(s)r(t-s),
\]
where $l$ is the distribution of scintillation light at the end of the calorimeter as a function of time and $r$ is the time response of the photosensor to a single photon. The latter is experimentally determined from calibration of the apparatus; the former can be recovered by a ``deconvolution'' once $R$ is measured.

It is known, indeed, that given any two random variables $X$ and $Y$ and called $Z$ their sum, the following properties hold (see appendix \ref{appendix:cumulants}):
\[
	f_Z = f_X*f_Y ~\text{and}~ c_Z^{(k)} = c_X^{(k)}+c_Y^{(k)}~\text{for any}~k,
\]
where $f$ and $c^{(k)}$ are respectively the probability density function and the $k$-order cumulant of each random variable. Therefore, if all the cumulants of the single-photon response distribution $r$ and the total response $R$ are known, all the cumulants of the distribution for the incident light are known as well:
\begin{equation}
	c_l^{(k)} = c_R^{(k)}-c_r^{(k)};
	\label{eq:cumulant_subtract}
\end{equation}
since a distribution is uniquely defined by the sequence of its cumulants, $l$ is in principle recoverable.

\section{Testing the cumulant additivity property}\label{section:convolution_test}

To verify the practical applicability of the property of additivity of cumulants in numerical computations, a sequence of tests has been carried out on different distributions of known analytic form.

In the test, for each pair of chosen distributions the following sequence of steps is followed:
\begin{itemize}
	\item a (discrete) convolution $h$ of the two distributions $f$ and $g$ is calculated following a Monte Carlo convolution algorithm (described below);
	\item the sequences of moments $m_f$, $m_g$ and $m_h$ for the discretized distributions are calculated up to some finite order;
	\item the corresponding cumulants $c_f$, $c_g$ and $c_h$ are calculated;
	\item the discrepancy $\delta^{(k)} = m_f^{(k)} + m_g^{(k)} - m_h^{(k)}$ is calculated for each order $k$;
	\item the above steps are repeated while gradually increasing the number of sampling points in the Monte Carlo convolution.
\end{itemize}

Finally, for each order $k$ the discrepancy $\delta^{(k)}$ is plotted against the number of Monte Carlo sampling points. This procedure has been tested with couples of gaussian, uniform, exponential and laplace distributions: as shown in the sample graphs in figure \ref{fig:discrepancy_overview}, the precision of the calculation becomes worse when the order of the cumulant gets higher, but in all cases the discrepancy goes to zero as the number of samples increases.

\begin{figure}[t]
	\includegraphics[width=\textwidth]{convolution_tests/overview}
	\caption{\small{Discrepancies between the sequences of cumulants calculated by the additivity property and the values obtained by convolution, for different couples of functions in different classes.}}
	\label{fig:discrepancy_overview}
\end{figure}

\begin{figure}[t]
	\includegraphics[width=\textwidth]{convolution_tests/overview_relative}
	\caption{\small{Relative cumulant discrepancies (only the orders for which the cumulants are non-vanishing are plotted).}}
	\label{fig:discrepancy_relative_overview}
\end{figure}

The implemented convolution algorithm \cite{doi:10.1063/1.1142280} requires that the distributions $f$ and $g$ be discretized. Supposing $[t_1,t_2]$ and $[t_3,t_4]$ are the intervals in which $f$ and $g$ are respectively defined, the following steps are repeated:
\begin{itemize}
	\item a time value $t_f$ sampled (e.g. by rejection sampling) from the distribution $f$ and a time value $t_g$ is sampled from $g$;
	\item the time value $t_h=t_f+t_g$ is stored in a list.
\end{itemize}
Once a sufficiently high number of iterations has been completed, a histogram of the obtained $t_h$'s can be plotted, ranging from $t_1+t_3$ to $t_2+t_4$; its normalization yields an approximation of the convolution of $f$ and $g$.

From an error analysis applied to this algorithm \cite{montecarlo_error}, one can estimate that the $i$-th bin of the final histogram has an uncertainty given by:
\[
	\sigma_h^2 = \frac{n}{n-1}\left(n_i-\frac{n_i^2}{n}\right)
\]

\section{A case of signal reconstruction}

As a benchmarking for the possibility of reconstructing the light distribution, let us consider an ideal, one-dimensional case, where the needed distribution $l$ follows some theoretical parametrization and the single-photon response $r$ is taken from the literature \cite{pmt_spr}. In this case, the shower development was modeled by the Greisen parametrization \cite{shower_shape}:
\begin{equation}
	N_\gamma(X) = \frac{0.31}{\sqrt\beta}\exp\left[X\left(1-\frac{3}{2}\log\frac{3X}{X+2\beta}\right)\right],
	\label{eq:greisen}
\end{equation}
where $X$ is the (adimensional) depth in the detector as a number of radiation lengths.

This function returns the number of photons produced in the shower at position $x=X_0X$; the shape of the light distribution can then be obtained by some physical considerations.

Let us suppose the first incident particle enters the calorimeter on one end with ultrarelativistic velocity ($\beta\approx 1$) at the beginning of the measurement ($t=0$), while the photosensor is placed at the opposite end. The instant a photon reaches the sensor is the sum of the time of travel of the charged particle before the emission $t_1$ and the time of travel of the photon itself from its production point $t_2$; if the calorimeter has total length $L$, photons produced at position $x$ will travel to the sensor through a path of length $L-x$ at speed $v=c/n$. Therefore:
\begin{equation}
	t = t_1+t_2 = \frac{x}{c}+\frac{L-x}{v} = \frac{L}{v}+\left(\frac{1}{c}-\frac{1}{v}\right)x;
	\label{eq:x_to_t}
\end{equation}
a photon will strike the sensor at least at time $L/c$ (if it is produced at the end of the scintillator) and at most at time $L/v$ (if it is produced at the beginning).

The light time distribution is then related to eq.~\eqref{eq:greisen} by a linear variable change. Fig.~\ref{fig:greisen} shows different shapes for a PWO omogeneous electromagnetic calorimeter as the velocity of the incident particle changes. Setting $\beta=1$ and calculating the convolution between the corresponding distribution and the single-photon response plotted in fig.~\ref{fig:spr_pmt} -- by applying the Monte Carlo convolution algorithm described in section -- one obtains the distribution shown in fig.~\ref{fig:pmt}, which is supposedly the distribution one would measure in an actual experiment.

\begin{figure}[b]
	\includegraphics[width=\textwidth]{pmt_signal/greisen}
	\caption{\small{On the left, the longitudinal shape of the shower is plotted as a function of the number of radiation lengths, for different velocities of the incident particle (eq.~\ref{eq:greisen}). On the right, time-dependent shower shapes at the surface of the photosensor. The latter is obtained from eq.~\ref{eq:greisen} by applying the variable change in eq.~\ref{eq:x_to_t} for a PWO calorimeter used in the CMS experiment ($X_0=\SI{0.92}{\centi\m}$ and $L=25X_0$).}}
	\label{fig:greisen}
\end{figure}

\begin{figure}[t]
	\includegraphics[width=\textwidth]{pmt_signal/spr_pmt}
	\caption{\small{Single-photon response for a 56 DVP-03 photomultiplier \cite{pmt_spr}.}}
	\label{fig:spr_pmt}
\end{figure}

\begin{figure}[p]
	\includegraphics[width=.9\textwidth]{pmt_signal/pmt}
	\caption{\small{Photomultiplier output signal in response to an electromagnetic shower, as obtained by convoluting the single-photon response and the shower shape in Greisen's parametrization.}}
	\label{fig:pmt}
\end{figure}

\begin{figure}[p]
	\includegraphics[width=.9\textwidth]{pmt_signal/pmt_discrepancies}
	\caption{\small{Relative discrepancies for the calculated cumulant sequence, for orders from 0 to 7.}}
	\label{fig:cumulant_diff}
\end{figure}

The next step is to calculate the cumulants of the scintillation light distribution both numerically (by integrating equation \eqref{eq:greisen}) and by using the additivity property, i.e. applying eq.~\eqref{eq:cumulant_subtract}. Fig.~\ref{fig:cumulant_diff} plots the relative discrepancy between the two results for each cumulant order from 1 to 5\footnote{An entry for order $0$ is included in the plot; however, for normalized distributions the zeroth cumulant is always $0$, similarly to how the zeroth moment is always $1$.}; it is apparently increasing with the order, though always remaining lower than 5\% with $10^6$ Monte Carlo sampling points.

Once the sequence of cumulants has been obtained up to a reasonable order, it is possible to try an approximation of the original distribution by Fourier-transforming the truncated series expansion of the cumulant generating function%, as described in appendix \ref{appendix:fourier}
. However, for such a reconstruction a second technique appears less computationally intensive and more precise, namely a series development in terms of orthogonal polynomials. The result of this procedure -- which is described in appendix \ref{appendix:jacobi} -- is shown in fig.~\ref{fig:light_comparison}, where it is also compared with the original Greisen parametrization; fig.~\ref{fig:dist_diff} plots the relative discrepancy between the two shapes.

\begin{figure}[p]
	\includegraphics[width=.9\textwidth]{pmt_signal/light_comparison}
	\caption{\small{Recreated light distribution obtained from the cumulants, compared with the original shower shape (Greisen's parametrization with $\beta=1$).}}
	\label{fig:light_comparison}
\end{figure}

\begin{figure}[p]
	\includegraphics[width=.9\textwidth]{pmt_signal/recreated_discrepancies}
	\caption{\small{Relative discrepancy between the original and the recreated light distributions.}}
	\label{fig:dist_diff}
\end{figure}

After the process, a second reconstruction technique, based on a fit to an analytical parameterization, has been tentatively applied. Supposing the moments from order 1 to $k$ are known, a fit to a generic function $f(x,\vec{p})$ depending on some set of parameters $\vec{p}$ consists of minimizing the functional
\begin{equation}
	S(\vec{p}) = \sum_{i=0}^k\left[m_k-\int_{-\infty}^{\infty}dx\,x^kf(x,\vec{p})\right]^2.
	\label{eq:least_square}
\end{equation}

In this case the chosen parameterization was of the form $x^\alpha\,e^\beta$: by letting $\alpha$ and $\beta$ vary both between 1 and 10 at steps of $0.1$ the best fit was found at $\alpha=1.54$ and $\beta=1$; the original and fitted shower shapes are compared in fig.~\ref{fig:dist_fit}.

\begin{figure}[p]
	\includegraphics[width=.9\textwidth]{images/graphs/pmt_signal/recreated_fit.png}
	\caption{\small{Original shower shape compared to the one reconstructed by least square fit.}}
	\label{fig:dist_fit}
\end{figure}

With respect to the Jacobi expansion approximation, the plot shows a lower precision which does not increase if one refines the granularity in the variations of $\alpha$ and $\beta$; this is certainly a limitation in the chosen parameterization. In general, this result confirms that a fit by some specific shape can only be achieved in some limited cases, when the shape of the expected distribution is already known to some extent.

\clearpage
\appendix
\addappheadtotoc

\section{Proof of the additivity of cumulants}\label{appendix:cumulants}
Given two random variables $X$ and $Y$ described respectively by the PDFs $f_X$ and $f_Y$, the random variable $Z=X+Y$ is described by the convolution
\[
	f_Z(x) = (f_X*f_Y)(x) = \int_{-\infty}^{+\infty} dy\, f_X(y)f_Y(x-y).
\]

The moments of any random variable appear in the power series expansion of its characteristic function
\[
	\phi_X(\omega) = \int_{-\infty}^{+\infty}dx\, e^{i\omega x}f_X(x) = \sum_{k=0}^\infty \frac{(i\omega t)^k}{k!}m_X^{(k)}.
\]

Cumulants, instead, are defined by taking the logarithm of the characteristic function and expanding it in series as well:
\begin{equation}
	\kappa(\omega) = \log(\phi_X(\omega)) = \sum_{n=0}^\infty \frac{(i\omega t)^n}{n!}c_X^{(n)}.
\end{equation}
$K$ is usually called \emph{cumulant generating function} (CGF).

For moments and cumulants, therefore, the following definitions respectively hold:
\begin{equation}
	m_X^{(n)} = \frac{1}{i^n}\frac{\partial^n\phi_X}{\partial \omega^n}\Bigr|_{\substack{\omega=0}}
	~\text{and}~
	c_X^{(n)} = \frac{1}{i^n}\frac{\partial^n \kappa_X}{\partial \omega^n}\Bigr|_{\substack{\omega=0}}.
\label{eq:cumulants}
\end{equation}

The first two cumulants are the well-known mean and variance, while the third- and fourth-order cumulants are skewness and kurtosis; they can be expressed in terms of lower-order moments, as follows:
\[\begin{split}
	c_1 &= m_1, \\
	c_2 &= m_2 - m_1^2, \\
	c_3 &= m_3 - 3m_2m_1 + 2m_1^3, \\
	c_4 &= m_4 - 4m_3m_1 - 3m_2^2 + 12m_2m_1^2 - 6m_1^4.
\end{split}\]

By iteratively differentiating the characteristic function, instead, one can get a recursive relation for moments and cumulants, useful for computations \cite{moments_and_cumulants}:
\[\begin{split}
	m_n = \sum_{k=0}^{n-1}\binom{n-1}{k}m_k~c_{n-k}, \\
	c_n = m_n-\sum_{k=1}^{n-1}\binom{n-1}{k}m_k~c_{n-k}.
\end{split}\]

Since characteristic functions verify the multiplication property
\[ \phi_{X+Y} = \phi_X\,\phi_Y, \]
for CGFs one has
\[ \kappa_{X+Y}=\kappa_X+\kappa_Y. \]
The linearity of cumulants follows directly from this observation by applying the second part of eq.~\eqref{eq:cumulants}.

This property lies at the basis of the procedure described in the present report, as the incident particle distribution is obtained from the sequence of its cumulants, which in turn are obtained by simple subtraction between the cumulants of the observed shower shape and the ones of the calibration data.

%\section{Recreating a distribution from the sequence of its cumulants}\label{appendix:fourier}

\section{Approximating a distribution from its moments}\label{appendix:jacobi}

Sets of orthogonal polynomials forming a basis of the $L^2$ function space can be used to express $L^2$ functions in a limited interval as a series expansion. This property can be used to recover the shape of a distribution when its moments are known \cite{distribution_approximation}.

Let us limit for simplicity to a distribution $f$ in the interval $[-1,1]$. The normalized Jacobi polynomials, which can be written in terms of their matrix of coefficients $\{G_{ij}\}$ as
\begin{equation}
	G_i(s) = \sum_{j=1}^iG_{ij}\,s^j,
	\label{eq:jacobi1}
\end{equation}
form an orthogonal set in this interval, that is:
\[ \int_{-1}^1ds\, G_i(s)\,G_j(s) = \delta_{ij}. \]

Therefore, by writing the distribution $f$ as linear combination of Jacobi polynomials with respect to some coefficients $\{\lambda_i\}$,
\[
	f(s) = \sum_{j=0}^\infty\lambda_j\,G_j(s),
\]
we can recover the $i$-th order coefficient by calculating the following scalar product:
\begin{equation}
	\int_{-1}^1ds\, f(s)\,G_i(s) = \sum_{j=0}^\infty\lambda_j\int_{-1}^1ds\,G_j(s)\,G_i(s) = \lambda_i.
	\label{eq:jacobi2}
\end{equation}

Subsequently, by writing the $G_i$'s explicitly (i.e. by applying eq.~\eqref{eq:jacobi1} to the left-hand side of eq.~\eqref{eq:jacobi2}) we get a simple algebraic relation between the $\lambda_i$'s and the first $i$ moments of $f$:
\[
	\lambda_i = \sum_{j=0}^i\int_{-1}^1ds\,f(s)\,G_{ij}\,s^j = \sum_{j=0}^iG_{ij}\,m_j.
\]

The following series -- truncated to the highest known moment order n -- is then an approximation for the shape of $f$:
\[
	f_n(s) = \sum_{i=0}^n\Bigg(\sum_{j=0}^iG_{ij}\,m_j\Bigg)G_i(s).
\]

To generalize this conclusion to any distribution $f$ defined in some generic interval $[t_0,t_1]$, one needs to:
\begin{itemize}
	\item consider a new distribution $g$ which corresponds to a ``shrunk'' copy of $f$ to the interval $[-1,1]$ (through a linear variable change);
	\item calculate the moments $\{m_g^{(j)}\}$ of $g$ from the known moments $\{m_f^{(j)}\}$;
	\item recreate $g$ following the procedure above;
	\item expand back $g$ by inverting the variable change to recover $f$.
\end{itemize}

It is straightforward to prove, using Newton's binomial theorem, that the moments of $f$ and $g$ are related by:
\[
	m_g^{(k)} = \sum_{j=0}^k\binom{k}{j}\left(\frac{2}{t_1-t_0}\right)^{k-j+1}\,
	\left(1-\frac{2t}{t_1-t_0}\right)^jm_f^{(k-j)}.
\]
This is the main reconstruction procedure used to perform the desired ``deconvolution'' in this work.

\clearpage

\bibliographystyle{apalike}
\bibliography{reference}

\end{document}